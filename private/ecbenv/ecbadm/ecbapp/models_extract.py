# -*- coding: UTF-8 -*-

...

# ckeditor
from ckeditor.fields import RichTextField
from ckeditor_uploader.fields import RichTextUploadingField

# models
class Ip(models.Model):
    INET_CHOICES = (
        ('inet', 'IPv4'),
        ('inet6', 'IPv6'),
    )
    STATUS_CHOICES = (
        ('0', _('Not configured')),
        ('1', _('SMTP: En cours d\'ajout')),
        ('2', _('On line')),
        ('3', _('SMTP: En cours de retrait')),
        ('4', _('SMTP: Deleted')),
        ('5', _('Off line')),
        ('6', _('REGISTAR: En commande')),
        ('7', _('REGISTAR: En cours d\'effacement')),
        ('8', _('Archived')),
    )
    STATUS_ADM = (
        ('0', _('En commande')),
        ('1', _('Waiting configuration')),
        ('2', _('Configured')),
        ('3', _('Waiting deletion')),
        ('4', _('Archived')),
    )
    FUNCTION_CHOICES = (
        ('0', _('System')),
        ('1', _('MX')),
        ('2', _('Routing')),
    )
    DOMR_STATUS = (
        ('0', _('Ignored')),
        ('1', _('Being configured')),
        ('2', _('Configured')),
        ('3', _('Being removed')),
    )

    date_created = models.DateTimeField(_("Date created"), auto_now_add=True, db_index=True)
    date_updated = models.DateTimeField(_("Date updated"), auto_now=True, db_index=True)
    iface = models.CharField(max_length=10, blank=True, null=True, db_index=True)
    ifacealiasnumber = models.PositiveIntegerField(null=True, blank=True, db_index=True)
    version = models.CharField(max_length=5, choices=INET_CHOICES, default='inet')
    address = models.CharField(max_length=45, blank=True, null=True, unique=True, db_index=True)
    netmask = models.CharField(max_length=45, blank=True, null=True)
    gateway = models.CharField(max_length=45, default='')
    broadcast = models.CharField(max_length=45, null=True, blank=True)
    network = models.CharField(max_length=45, default='')
    nodets = models.ForeignKey("NodeTS", on_delete=models.CASCADE, related_name='ips', verbose_name=_("Transit Server (SMTP)"))
    nodetm = models.ForeignKey("NodeTM", null=True, blank=True, on_delete=models.PROTECT, related_name='ips', verbose_name=_("Master Server"))
    status = models.CharField(max_length=2, choices=STATUS_CHOICES, default='0')
    status_adm = models.CharField(max_length=2, choices=STATUS_ADM, default='0', db_index=True)
    status_ping = models.BooleanField(default=False)
    status_smtp = models.BooleanField(default=False)
    status_hoster = models.BooleanField(default=False)
    hetrixtools_result = models.CharField(max_length=255, default='')
    hetrixtools_date_updated = models.DateTimeField(_("Date updated"), null=True, auto_now=False, db_index=True)
    domr_status = models.CharField(max_length=2, choices=DOMR_STATUS, default='1')
    auto = models.BooleanField(default=False)
    function = models.CharField(max_length=2, choices=FUNCTION_CHOICES, default='2', verbose_name=_("Function"))
    warming_up_speed = models.PositiveIntegerField(null=True, verbose_name=_("Quota horaire"))
    warming_up_stats = models.PositiveIntegerField(null=True)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='ips', null=True, blank=True, on_delete=models.SET_NULL, verbose_name=_("User"))
    shortname = models.CharField(max_length=255, blank=True, default='')
    dns_domain = models.ForeignKey("Domain", related_name='dnsarecords', null=True, blank=True, on_delete=models.PROTECT)
    mailtester = models.BooleanField(default=False)
    rotation_group = models.ForeignKey("RotationGroup", related_name='ips', null=True, blank=True, on_delete=models.SET_NULL)
    comment = models.TextField(verbose_name=_("Comment"), null=True, blank=True)

    def __unicode__(self):
        return u"%s - %s - %s - %s - %s - %s - %s" % (self.nodets, self.iface, self.address, self.netmask, self.gateway, self.reverse, self.get_status_adm_display())

    class Meta:
        verbose_name = _('IP')
        verbose_name_plural = _('IPs')
        ordering = ['iface']
        unique_together = ("nodets", "address")
        unique_together = ("nodets", "ifacealiasnumber")

    @property
    def realreverse(self):
        my_resolver = dns.resolver.Resolver()

        if getattr(settings, 'ECB_DNS_NAMESERVERS' , None):
            my_resolver.nameservers = settings.ECB_DNS_NAMESERVERS

        try:
            addr = reversename.from_address(self.address)
            ptr = str(my_resolver.query(addr, "PTR")[0])
            if ptr.endswith('.'):
                ptr = ptr[:-1]
            return ptr
        except:
            return ''

    @property
    def reverse(self):
        if self.function == '0' and self.nodets:
            return self.nodets.hostname
        elif self.dns_domain:
            return self.shortname+'.'+self.dns_domain.name
        else:
            return ''

    @property
    def new_reputations_display(self):
        try:
            res_reputations = {'1': {}, '2': {}, '3': {}, 'ok': 0}
            str_reputation = ''
            for reputation in self.reputation_rbls.all().exclude(rbl__status='0'):
                if reputation.is_ok():
                    res_reputations['ok'] += 1
                elif reputation.result not in res_reputations[reputation.rbl.status]:
                    res_reputations[reputation.rbl.status][reputation.result] = 1
                else:
                    res_reputations[reputation.rbl.status][reputation.result] += 1
            
            for key in res_reputations.keys():
                if key == 'ok':
                    str_reputation += '<span class="bg-success">' + 'OK (' + str(res_reputations['ok']) + ')'
                else:
                    if key == '2':
                        str_reputation += '<span class="bg-warning">'
                    else:
                        str_reputation += '<span class="bg-danger">'
                    for msg in res_reputations[key].keys():
                        str_reputation += msg + ' (' + str(res_reputations[key][msg]) + ')'
                str_reputation += '</span>'
                
            return str_reputation
        except:
            return ''

    @property
    def reputations(self):
        try:
            reputations = 'OK'
            for reputation in self.reputation_rbls.filter(Q(rbl__status='1')|Q(rbl__status='3')):
                if reputation.is_ok() and reputations == '':
                    reputations = 'OK'
                elif not reputation.is_ok() and reputations == 'OK':
                    reputations = reputation.result
                elif not reputation.is_ok():
                    reputations += "\n"+reputation.result
            return reputations
        except:
            return ''

    @property
    def hetrixtools_result_display(self):
        if self.hetrixtools_result == 'OK':
            return '<span class="fa fa-check-circle statusok"></span>'
        else:
            return self.hetrixtools_result
        return ''

    @property
    def is_v4(self):
        try:
            socket.inet_pton(socket.AF_INET, self.address)
            return True
        except socket.error:
            return False

    @property
    def inet(self):
        if self.address:
            try:
                socket.inet_pton(socket.AF_INET, self.address)
                return 'inet'
            except socket.error:
                return 'inet6'
        else:
            return ''

    def set_shortname(self):
        tosave = False

        if self.nodets:
            if self.function == '0':
                ext = 'sys'
            if self.function == '1':
                ext = 'in'
            if self.function == '2':
                ext = 'out'
            shortname = self.nodets.rootshortname + ext + '-' + str(self.ifacealiasnumber)
        else:
            shortname = ''
        if self.shortname != shortname:
            self.shortname = shortname
            tosave = True
        return tosave

    def set_aliases(self, iface):
        tosave = False

        if self.ifacealiasnumber == None:
            # on cherche un ordernumber
            ips = Ip.objects.filter(nodets=self.nodets)
            ordernumber = 1
            while True:
                checkordernumber = True
                for getip in ips:
                    if getip.ifacealiasnumber == ordernumber:
                        checkordernumber = False
                        break
                if checkordernumber:
                    break
                ordernumber += 1
            self.ifacealiasnumber = ordernumber
            tosave = True

        if self.function != '0':
            iface = iface + ':' + str(self.ifacealiasnumber)

        if iface != self.iface:
            self.iface = iface
            tosave = True

        return tosave

    def check_rbl(self, debug=False):
        batchlog_fctname = 'ip.check_rbl'

        if debug:
            print '============================================='
            print self.address

        my_resolver = dns.resolver.Resolver()

        if getattr(settings, 'ECB_DNS_NAMESERVERS' , None):
            my_resolver.nameservers = settings.ECB_DNS_NAMESERVERS

        rbls = RBL.objects.exclude(status=0)
        for rbl in rbls:
            if debug:
                print '--------------------------------------------'
                print rbl.dns
            answer = ''
            query = ''
            try:

                query = '.'.join(reversed(self.address.split("."))) + "." + rbl.dns
                if debug:
                    print 'query:' + query
                try:
                    answers = my_resolver.query(query, "TXT")
                    for rdata in answers:
                        for string in rdata.strings:
                            answer += string
                except dns.resolver.NoAnswer:
                    answers = my_resolver.query(query, "A")
                    answer = 'No Answer'
            except dns.resolver.NXDOMAIN:
                answer = 'OK'
            except dns.resolver.NoNameservers:
                answer = 'No Nameservers'
            except dns.resolver.NoAnswer:
                answer = 'No Answer'
            except dns.exception.Timeout:
                answer = 'Timeout'

            if debug:
                print 'answer: ' + answer

            try:
                reputation = Reputation.objects.get(ip=self,rbl=rbl)
                reputation.result = answer
            except ObjectDoesNotExist:
                reputation = Reputation(ip=self,rbl=rbl,result=answer)
            reputation.save()

            # LOG
            if answer == 'OK' or answer == 'No Nameservers' or answer == 'No Answer' or answer == 'Timeout':
                batchlog_status = '2'
            else:
                batchlog_status = '1'
            BatchLog.objects.create(status=batchlog_status, fctname=batchlog_fctname, log='RBL: ' + rbl.dns + ' - QUERY: ' + query + ' - ANSWER: ' + answer, ip=self)

            # cas blocage IP si mauvaise reputation
            if rbl.status == '3' and answer != 'OK' and answer != '' and answer != 'Timeout' and answer != 'No Answer' and answer != 'No Nameservers' and self.auto :
                self.auto = False
                self.save()

                # envoi un mail de notification
                message = mail.EmailMultiAlternatives(
                    subject='blocage IP ' + ip.address,
                    body=ip.address,
                    from_email=settings.ENVELOPE_FROM_EMAIL,
                    to=settings.ECB_NOTIFICATIONS
                )
                message.send()

                # LOG
                BatchLog.objects.create(status='0', fctname=batchlog_fctname, log='RBL: ' + rbl.dns + ' - BLOCAGE IP', ip=self)

        # Nettoyage Reputation
        rbls = RBL.objects.filter(status=0)
        for rbl in rbls:
            try:
                reputation = Reputation.objects.get(ip=self,rbl=rbl)
                reputation.delete()
            except ObjectDoesNotExist:
                pass

    def hetrixtools_update(self, result, debug=False):
        batchlog_fctname = 'ip.hetrixtools_update'

        self.hetrixtools_result = ''

        if result['Blacklisted_On'] != None:
            for blt in result['Blacklisted_On']:
                if blt['RBL'] is None:
                    self.hetrixtools_result = 'OK'
                else:
                    if self.hetrixtools_result == '':
                        self.hetrixtools_result = blt['RBL']
                    else:
                        self.hetrixtools_result = self.hetrixtools_result + ' / ' + blt['RBL']

        else:
            self.hetrixtools_result = 'OK'

        self.hetrixtools_date_updated = timezone.now()
        self.save()

    def check_ptr(self):
        batchlog_fctname = 'ip.check_ptr'

        if self.realreverse != self.reverse:
            BatchLog.objects.create(status='1', fctname=batchlog_fctname, log='Reverse to update', ip=self)
            return False
        else:
            BatchLog.objects.create(status='2', fctname=batchlog_fctname, log='Reverse OK', ip=self)
            return True

    def update_ptr(self, debug=False):
        batchlog_fctname = 'ip.update_ptr'

        if self.function != '0':
            if self.shortname != '' and self.dns_domain:
                reverse = self.shortname+'.'+self.dns_domain.name+'.'
            else:
                if self.shortname == '':
                    BatchLog.objects.create(status='1', fctname=batchlog_fctname, log='No shortname', ip=self)
                if not self.dns_domain:
                    BatchLog.objects.create(status='1', fctname=batchlog_fctname, log='No domain name', ip=self)
                return False
        else:
            reverse = self.nodets.hostname+'.'

        batchlog_log = 'Reverse: ' + reverse

        if self.nodets.hoster == '0':
            # GLESYS
            api = glesysApi(settings.GLESYS_URL, settings.GLESYS_USERNAME, settings.GLESYS_KEY)
            response = api.post('/ip/setptr', {"ipaddress": self.address, "data": reverse})
            status='0'

        if self.nodets.hoster == '1':
            # FH
            fh = restapi_client(hoster_url=settings.FH_URL, hoster_uid_kname='LOGIN', hoster_uid=settings.FH_LOGIN, hoster_api_key_kname='TOKEN', hoster_api_key=settings.FH_TOKEN, debug=debug)
            try:
                response = fh.post('/ip/reverse', {"service": self.address, "reverse": reverse})
            except:
                print ('problem API')
                BatchLog.objects.create(status='1', fctname=batchlog_fctname, log=batchlog_log, ip=self)
                return False

            if not response['result']:
                BatchLog.objects.create(status='1', fctname=batchlog_fctname, log=batchlog_log, ip=self)
                return False

        if self.function != '0':
            # Reconfiguration du domaine du reverse
            if self.dns_domain.status_adm != '4':
                self.dns_domain.status_adm = '1'
                self.dns_domain.status_dns = False
                self.dns_domain.save()
                BatchLog.objects.create(status='2', fctname=batchlog_fctname, log='Passage du domaine ' + self.dns_domain.name + ' en statut configuration', ip=self)

        # LOG
        BatchLog.objects.create(status='0', fctname=batchlog_fctname, log=batchlog_log, ip=self)

        return True

    def availability_status_for_mailtester(self):
        if not self.status_ping or not self.status_smtp or not self.status_hoster or self.function != '2':
            return False

        return True

    def availability_status_timeperiod(self):
        if self.rotation_group:
            return self.rotation_group.timeperiod()
        return True

    def availability_status_quota(self):
        # calcul du quota de la dernière heure
        if self.warming_up_speed:
            h_now = timezone.localtime(timezone.now())
            h_before = h_now - timedelta(hours=1)
            if Spool.objects.filter(ip=self, sent__lt=h_now, sent__gte=h_before).count() > self.warming_up_speed:
                return False

        return True

    def availability_status(self):
        if not self.availability_status_for_mailtester() or not self.availability_status_timeperiod():
            return False

        if self.reputations != 'OK' or self.auto == False or self.nodets.smtpactive >= settings.ECB_SMTP_LIMIT:
            return False

        if settings.HETRIXTOOLS:
            if self.hetrixtools_result != 'OK' and self.hetrixtools_result != '':
                return False

        if not self.availability_status_quota():
            return False

        return True

    def check_ping(self):
        if self.is_v4:
            log_msg = 'ping IPv4'
            ret = ping_return_code(self.address)
        else:
            log_msg = 'ping IPv6'
            ret = ping6_return_code(self.address)
        if ret == 0:
            log_status = '2'
            if not self.status_ping:
                self.refresh_from_db()
                self.status_ping = True
                self.save()
        else:
            log_status = '1'
            if self.status_ping:
                self.refresh_from_db()
                self.status_ping = False
                self.save()

        # LOG
        BatchLog.objects.create(status=log_status, fctname='Ip.check_ping', log=log_msg, ip=self)

    def check_smtp(self):
        if check_smtp(self.address):
            log_status = '2'
            if not self.status_smtp:
                self.refresh_from_db()
                self.status_smtp = True
                self.save()
        else:
            log_status = '1'
            if self.status_smtp:
                self.refresh_from_db()
                self.status_smtp = False
                self.save()

        # LOG
        BatchLog.objects.create(status=log_status, fctname='Ip.check_smtp', log='', ip=self)

    def delete_hoster(self):
        # rend l'IP chez l'hébergeur

        if self.nodets.hoster == '0':
            # GLESYS

            api = glesysApi(settings.GLESYS_URL, settings.GLESYS_USERNAME, settings.GLESYS_KEY)

            try:
                response = api.post('/ip/details', {"ipaddress": self.address})
                # On enlève la rattachement de l'IP au serveur
                response = api.post('/ip/remove', {"ipaddress": self.address})
                # On vérifie que l'IP n'est plus attachée à un serveur
                response = api.post('/ip/details', {"ipaddress": self.address})
                if response['details']['serverid'] == None:
                    # On rend l'IP à Glesys
                    response = api.post('/ip/release', {"ipaddress": self.address})
                    # On vérifie que l'IP est bien disparue
                    response = api.post('/ip/details', {"ipaddress": self.address})
                    if response['details']['reserved'] == 'no':
                        # On archive l'IP
                        self.status_adm = 4
                        self.status_hoster = False
            except requests.exceptions.HTTPError:
                # On archive l'IP
                self.status_adm = 4
                self.status_hoster = False

        if self.nodets.hoster == '1':
            self.status_adm = 4
            self.status_hoster = False

        self.save()

    def check_reverse(self):
        if self.reverse == self.realreverse:
            return True
        else:
            return False

import signals
