# -*- coding: UTF-8 -*-

# python
import logging
logger = logging.getLogger(__name__)
import sys, os
import random
import socket
import time
import inspect
import traceback
import magic
import urllib
import re
from datetime import timedelta

import email
from email.mime.base import MIMEBase
from email.MIMEMultipart import MIMEMultipart
from email.MIMEText import MIMEText
from email.MIMEImage import MIMEImage
from email import Charset, encoders
from email.header import Header
import dkim
from Crypto.PublicKey import RSA

# django
from django.core.management.base import BaseCommand, CommandError
from ecbapp.models import Spool, Domain, Ip, Econf
from ecbapp.lib.smtp import sendmail
from ecbapp.utils import id_generator, current_milli_time, recordtime
from django.core.cache import cache
from django.conf import settings
from django_q.tasks import async, result, fetch, Async
from django.utils import timezone
from django.db.models import Q
from django.core.exceptions import ValidationError
from django.core.validators import validate_email

# commands

def sendmail_result(task):
    if not task.success:
        logger.error(task.result)

def process_spool(spool, spoolrandomexec, hostname, maxtime, options):

    # Mesure du temps d'execution
    measureok = cache.get('taskmss_measure')
    if measureok == None:
        cache.set('taskmss_measure', 1, 300)
        logmeasurestart = current_milli_time()

    fctlogname = '== '+inspect.stack()[0][3]+' ==> '

    if options['test']:
        print (fctlogname+'-> process')

    # On vérifie que la tâche a le droit d'envoyer le message
    value = cache.get('spoolrandomexec_'+str(spool.id))
    if value == None or value != spoolrandomexec:
        logger.error(fctlogname+'spoolrandomexec ('+str(spoolrandomexec)+') vide ou ne correspond pas à '+str(value)+' pour '+str(spool.id))
        return False

    # On vérifie si le temps limite pour démarrer la tâche est écoule
    if time.time() > maxtime:
        cache.delete('spoolrandomexec_'+str(spool.id))
        return False

    # On vérifie qu'on a un domaine d'envoi
    try:
        spool.domain.name
    except AttributeError:
        logger.error(fctlogname+'Aucun domaine d\'envoi pour : '+str(spool.id))
        # on efface le message
        spool.delete()
        return False

    # On vérifie qu'on a un uniqueId
    try:
        spool.uniqueId
        if spool.uniqueId == '':
            logger.error(fctlogname+'Aucun uniqueId pour : '+str(spool.id))
            # on efface le message
            spool.delete()
            return False
    except AttributeError:
        logger.error(fctlogname+'Aucun uniqueId pour : '+str(spool.id))
        # on efface le message
        spool.delete()
        return False

    # Charset
    Charset.add_charset('utf-8', Charset.QP, Charset.QP, 'utf-8')

    if options['test']:
        print (fctlogname+'-> build message')

    # MULTIALT : Initialisation du message
    incoming_multialt = MIMEMultipart('alternative')

    # Remplaclement des chaines du template
    incoming_multialt.attach(MIMEText(spool.txt_template_replace().encode('utf-8'), 'plain', 'utf-8'))
    incoming_multialt.attach(MIMEText(spool.html_template_replace().encode('utf-8'), 'html', 'utf-8'))
    incoming_main = incoming_multialt

    # RELATED : rien

    # MIXED : Initialisation du message si fichier attaché
    attachments = spool.message.messageattachedfiles.all()
    if len(attachments) > 0:
        incoming_mixed = MIMEMultipart('mixed')
        incoming_mixed.attach(incoming_main)

    for attachment in spool.message.messageattachedfiles.all():
        file_read_flags = "rb"
        try:
            mimestring = magic.from_file(settings.MEDIA_ROOT+attachment.file.name, mime=True)
            if mimestring.startswith('text'):
                file_read_flags = "r"
            mimestring_parts = mimestring.split('/')
            part_file = MIMEBase(mimestring_parts[0], mimestring_parts[1])
        except AttributeError, IndexError:
            # cannot determine the mimetype so use the generic 'application/octet-stream'
            part_file = MIMEBase('application', 'octet-stream')

        attachment.file.open(file_read_flags)
        part_file.set_payload(attachment.file.read())
        encoders.encode_base64(part_file)
        part_file.add_header('Content-Disposition', "attachment; filename= %s" % attachment.filename())
        incoming_mixed.attach(part_file)
    if len(attachments) > 0:
        incoming_main = incoming_mixed

    # MAIN : Ajout des entêtes
    incoming_main['From'] = "\"%s\" <%s>" % (Header(spool.message.identity.name, 'utf-8'), spool.message.identity.get_cache_email_userpart() + '@' + spool.domain.name)
    incoming_main['To'] = "\"%s\" <%s>" % (Header(spool.contact.email, 'utf-8'), spool.contact.email.encode('utf-8'))
    incoming_main['Subject'] = "%s" % Header(spool.message.subject, 'utf-8')
    incoming_main['Date'] = email.utils.formatdate(time.time(), localtime=True)
    incoming_main['Message-Id'] = re.sub(r'<(.*?)@(.*?)>', r'<\1@', email.utils.make_msgid('1')) + spool.domain.name + '>'
    incoming_main['List-Unsubscribe'] = '<' + spool.unsubscribe_url() + '>'

    # Construction du sender
    sender = spool.message.identity.get_cache_email_userpart() +'ret+' + spool.uniqueId + '@' + spool.domain.name

    # Récupération du domaine du sender
    senderdomainname = spool.domain.name

    # Recherche en CACHE
    domain = cache.get('domainobj_'+senderdomainname)

    if domain == None:
        # ===> Requête SQL
        domainsearch = Domain.objects.filter(name=senderdomainname)

        if len(domainsearch) > 0:
            domain = domainsearch[0]
            cache.set('domainobj_'+senderdomainname, domain, 60)
        else:
            logger.error(fctlogname+'Nom de domaine pas dans la base pour '+str(spool.id))
            return False

    # Routage
    if domain.smtprelay is None:
        if domain.dkim_domainkey_private:
            sig = dkim.sign(incoming_main.as_string(), 'default', domain.name, re.sub('\r', '', domain.dkim_domainkey_private))
            incoming_main['DKIM-Signature'] = sig[len("DKIM-Signature: "):]
        else:
            logger.debug(fctlogname+'Pas de cle privee')
            return False

        if options['test']:
            print (fctlogname+'-> search route')

        # RELAYHOST : recherche d'une IP relais aléatoire
        # Lecture des routes possibles
        if spool.ip:
            relayhost = spool.ip.address
        else:
            # Récupération du relais :

            # Recherche en CACHE
            routings = cache.get('routingsobj_'+senderdomainname+'_'+hostname)

            if routings == None:
                # ===> Requête SQL
                routings = domain.routings.filter(active=True)
                cache.set('routingsobj_'+senderdomainname+'_'+hostname, routings, 60)

            if options['test']:
                print (fctlogname+'-> Nb de routes dispos : '+str(len(routings)))

            # Tirage au sort d'un numéro de relais
            if len(routings) < 1:
                logger.warning(fctlogname+'Aucune route pour '+str(spool.id))
                return False
            relaynb = random.randint(1, len(routings))
            try:
                routing = routings[(relaynb-1)]
            except IndexError:
                logger.warning(fctlogname+'Erreur de choix de route pour '+str(spool.id))
                return False

            relayhost = routing.ip.address
            spool.ip = routing.ip
        relayuser = None
        relaypassword = None
    else:
        relayhost = domain.smtprelay.hostname
        relayuser = domain.smtprelay.username
        relaypassword = domain.smtprelay.password

    # mesure du temps d'envoi d'un mail
    if measureok == None:
        logmeasuresendmailstart = current_milli_time()
        logmeasuresendmailtime = 0

    try:
        spool.contact.email.decode('ascii')
    except UnicodeEncodeError:
        # on blacklist le contact
        spool.contact.noemail = True
        spool.contact.save()
        spool.delete()
    else:
        if options['test']:
            print (fctlogname+'-> sending')

        try:
            # SEND
            if sendmail(sender, spool.contact.email, incoming_main, relayhost, relayuser, relaypassword):
                # mesure du temps d'envoi d'un mail
                if measureok == None:
                    logmeasuresendmailtime = current_milli_time() - logmeasuresendmailstart
                logger.debug(fctlogname+'Mail OK')

                # On enregistre le message comme envoyé
                spool.sent = timezone.now()
                spool.save()
            else:
                logger.debug(fctlogname+'Erreur d\'envoi')
                try:
                    validate_email(spool.contact.email)
                except ValidationError as e:
                    spool.contact.noemail = True
                    spool.contact.save()
                    spool.delete()
        except UnicodeEncodeError:
            spool.delete()

    # mesure du temps d'execution
    if measureok == None:
        logmeasuretime = current_milli_time() - logmeasurestart

        # enregistrement des mesures
        recordtime(logmeasuresendmailtime,os.path.join(settings.ECB_MEASURE_DIR, 'messages-spool-scan-process_spool-logmeasuresendmailtime.txt'))
        recordtime(logmeasuretime,os.path.join(settings.ECB_MEASURE_DIR, 'messages-spool-scan-process_spool-logmeasuretime.txt'))

    return True

def process_all_spools(spools, econf, hostname, options):
    fctlogname = '== '+__name__+' ==> '

    if options['test']:
        print (fctlogname+'-> TEST MODE')

    if options['log']:
        print "[ Log ] In process_all_spools -> Entering ... "

    for spool in spools:

        # on copie le domaine utilisé pour l'envoi
        spool.backupDomain()

        if not spool.domain:
            if options['debug']:
                print (fctlogname+'Pas de domaine')
            continue

        if options['debug']:
            print (fctlogname+'Traitement du spool : '+str(spool.id))

        if options['log']:
            print "[ Log ] In process_all_spools/loop -> spool.id = " + str(spool.id)
            print "[ Log ]                            -> spool.domain.smtprelay = " + str(spool.domain.smtprelay)
            print "[ Log ]                            -> spool.campaign = " + str(spool.campaign)

        # Traitement du taux d'envoi par seconde
        if spool.domain.smtprelay:
            if spool.domain.smtprelay.rate > 0:

                if options['log']:
                    print "[ Log ] In process_all_spools/loop/sendrate_calculus -> Entering ... "
                    print "[ Log ]                                              -> spool.domain.smtprelay.rate = " + str(spool.domain.smtprelay.rate)

                while True:

                    if options['log']:
                        print "[ Log ] In process_all_spools/loop/sendrate_calculus/while -> Looping ... "

                    # Calcul du taux

                    # Lecture de la liste des spool id à chercher en cache
                    sendrate_all = cache.get('sendrate_all_' + str(spool.domain.smtprelay.id))

                    if sendrate_all == None:
                        sendrate_all = []

                    sendrate_all_cpt = 0

                    if options['test']:
                        print (sendrate_all)

                    tmz = timezone.now()

                    # Lecture de la date d'envoi des spool récents
                    for spool_id in sendrate_all:
                        sendrate_spool = cache.get('sendrate_spool_' + str(spool_id))

                        if options['test']:
                            print (sendrate_spool)

                        if sendrate_spool != None:
                            if sendrate_spool > (tmz-timedelta(seconds=1)):
                                sendrate_all_cpt = sendrate_all_cpt + 1
                            else :
                                # date envoi trop vieille, on enlève de la liste des spool à calculer
                                sendrate_all.remove(spool_id)

                    if sendrate_all_cpt > spool.domain.smtprelay.rate:
                        logger.debug(fctlogname+'NoK / Rate : relay / cpt : ' + spool.domain.smtprelay.name + ' / ' + str(sendrate_all_cpt))
                        time.sleep(0.5)
                    else:
                        logger.debug(fctlogname+'Ok / Rate : relay / cpt : ' + spool.domain.smtprelay.name + ' / ' + str(sendrate_all_cpt))

                        sendrate_all.append(spool.id)
                        cache.set('sendrate_all_' + str(spool.domain.smtprelay.id), sendrate_all, 10)
                        cache.set('sendrate_spool_' + str(spool.id), tmz, 10)
                        break

                    if options['test']:
                        print (sendrate_all)

                    if options['log']:
                        print "[ Log ] In process_all_spools/loop/sendrate_calculus/while -> ... sendrate_all : " + str(sendrate_all)


        if spool.campaign:
            timeperiodok = cache.get('timeperiodok_'+str(spool.campaign.id))

            if options['log']:
                    print "[ Log ] In process_all_spools/loop/timeperiod_for_spoolcampaign -> timeperiodok = " + str(timeperiodok)

            if timeperiodok == None:
                timeperiodok = spool.campaign.is_timeperiod()
                cache.set('timeperiodok_'+str(spool.campaign.id), timeperiodok, 60)

        else:
            timeperiodok = True

        if timeperiodok:
            if options['debug']:
                print (fctlogname+'-> plage horaire OK')

            if options['log']:
                print "[ Log ] In process_all_spools/loop/in_timeperiod -> Entering ..."

            if options['test']:
                # dev
                spoolrandomexec = id_generator()
                cache.set('spoolrandomexec_'+str(spool.id), spoolrandomexec, settings.ECB_SPOOL_DIR_DJANGO_CACHE_TIMEOUT)
                process_spool(spool, spoolrandomexec, hostname, time.time()+(settings.ECB_SPOOL_DIR_DJANGO_CACHE_TIMEOUT-settings.ECB_SPOOL_DIR_BROKER_TIMEOUT), options)
            else:
                # spoolrandomexec sert à éviter qu'un message ne soit traité par deux instances, c'est un mot de passe
                spoolrandomexec = cache.get('spoolrandomexec_'+str(spool.id))

                if options['log']:
                    print "[ Log ] In process_all_spools/loop/in_timeperiod/randomexec -> spoolrandomexec = " + str(spoolrandomexec)

                if spoolrandomexec == None:
                    spoolrandomexec = id_generator()

                    cache.set('spoolrandomexec_'+str(spool.id), spoolrandomexec, settings.ECB_SPOOL_DIR_DJANGO_CACHE_TIMEOUT)
                    try:
                        if options['log']:
                            print "[ Log ] In process_all_spools/loop/in_timeperiod/randomexec/try_if_none -> spoolrandomexec = " + str(spoolrandomexec)

                        task_id = async('ecbapp.management.commands.messages-spool-scan.process_spool', spool, spoolrandomexec, hostname, time.time()+(settings.ECB_SPOOL_DIR_DJANGO_CACHE_TIMEOUT-settings.ECB_SPOOL_DIR_BROKER_TIMEOUT), options, hook='ecbapp.management.commands.messages-spool-scan.sendmail_result', timeout=settings.ECB_SPOOL_DIR_BROKER_TIMEOUT, cached=settings.ECB_SPOOL_DIR_BROKER_CACHED)

                        if options['log']:
                            print "[ Log ]                                                                 -> task_id = " + str(task_id)

                    except:
                        logger.error(fctlogname+'Erreur de lancement pour '+str(spool.id))
                        logger.error(traceback.format_exc())

            if options['log']:
                print "[ Log ] In process_all_spools/loop/in_timeperiod -> ... leaving."

        else:
            if options['debug']:
                print (fctlogname+'-> HORS plage horaire')

            if options['log']:
                print "[ Log ] In process_all_spools/loop/in_timeperiod -> Not in time period."

    if options['log']:
        print "[ Log ] In process_all_spools -> ... leaving."


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('-H', dest="hostname", help="Hostname", nargs='?')
        parser.add_argument('-d', dest="debug", help="debug", action="store_true")
        parser.add_argument('-t', dest="test", help="test", action="store_true")
        parser.add_argument('-i', dest="spoolid", type=int, help="spoolid", nargs='?')
        parser.add_argument('-c', dest="cpid", type=int, help="cpid", nargs='?')
        parser.add_argument('-l', dest="log", help="log", action="store_true")

    def handle(self, *args, **options):
        fctlogname = '== '+__name__+' ==> '

        # Nom d'hôte du serveur primaire d'envoi
        if options['hostname']:
            hostname = options['hostname']
        else:
            hostname = socket.getfqdn()

        # on cherche le nombre de spool à prendre à la fois
        econfs = Econf.objects.filter(name='messages-spool-scan_qty')
        if econfs.count() == 0:
            econf = Econf.objects.create(name='messages-spool-scan_qty', conf4=5000)
        else:
            econf = econfs[0]

        # Parcours du spoolid
        if options['spoolid'] > 0:
            spools = Spool.objects.filter(sent__isnull=True, pk=options['spoolid'])

        # Test d'une campagne en particulier
        if options['cpid']:
            # si on précise l'id de campagne (pour test), on ne prend que les 5 premiers spool
            spools = Spool.objects.filter(sent__isnull=True, campaign__isnull=False, campaign__pk=options['cpid']).order_by('date_created')
            spools = spools[:5]

        if not options['spoolid'] and not options['cpid']:
            # Parcours du spool SANS campagne
            spools = Spool.objects.filter(sent__isnull=True, campaign__isnull=True).order_by('-contact__mailtester','-contact__testadm','date_created')
            spools = spools[:econf.conf4]

            process_all_spools(spools, econf, hostname, options)

            # Parcours du spool AVEC campagne
            spools = Spool.objects.filter(sent__isnull=True, campaign__isnull=False, campaign__status='1').filter(Q(campaign__start_time__lte=timezone.now())|Q(campaign__start_time__isnull=True)).order_by('date_created')
            spools = spools[:econf.conf4]

        process_all_spools(spools, econf, hostname, options)

        sys.exit()
