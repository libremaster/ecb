# -*- coding: UTF-8 -*-

# python
...

# --------
# CAMPAIGN
# --------

class CampaignView(DetailView):
    model = Campaign

    def get_template_names(self):
        return ['ecbapp/campaign_detail.html']

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        obj = self.get_object()
        if not request.user.is_staff and obj.user != request.user:
            return HttpResponseForbidden()
        obj.update_stats_other()
        return super(CampaignView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        ctx = super(CampaignView, self).get_context_data(**kwargs)

        # Date premier message envoyé
        queryset = Spool.objects.filter(campaign=self.get_object())
        qs_order_date = queryset.filter(sent__isnull=False).order_by('sent')
        date_started_real = None
        if qs_order_date.count() > 0:
            date_started_real = qs_order_date[0].sent

        campaignlogs = CampaignLog.objects.filter(campaign=self.get_object())

        # Date de début par défaut
        first_message_sent = campaignlogs.all().order_by('date_started')
        if first_message_sent.count() > 0:
            date_started = first_message_sent[0].date_started
        else:
            date_started = timezone.now()

        # On arrondit la date de début
        date_started = date_started.replace(microsecond=0, second=0)

        # Date de début réel à afficher
        hstart = 60 * int(self.request.GET.get('start', 0))
        date_started = date_started+timedelta(minutes=hstart)
        date_started_minute = date_started.minute

        # Nombre de colonnes à afficher
        nbcol = 60

        # Date de fin à afficher
        date_ended = date_started + timedelta(minutes=nbcol+1)

        # Précision requetes
        campaignlogs = campaignlogs.filter(date_started__gte=date_started, date_ended__lt=date_ended)

        # Résultats
        res = []
        statsmax_stats_ecb_sent = 0
        statsmin_stats_ecb_sent = 0
        statsmax_stats_smtp_status = 0
        statsmin_stats_smtp_status = 0
        statsmax_stats_smtp_delivered = 0
        statsmin_stats_smtp_delivered = 0
        statsmax_stats_smtp_deferred = 0
        statsmin_stats_smtp_deferred = 0
        statsmax_stats_smtp_expired = 0
        statsmin_stats_smtp_expired = 0
        statsmax_stats_smtp_bounced = 0
        statsmin_stats_smtp_bounced = 0
        statsmax_stats_ecb_opened = 0
        statsmin_stats_ecb_opened = 0
        statsmax_stats_ecb_clicked = 0
        statsmin_stats_ecb_clicked = 0

        reslog = {}
        for campaignlog in campaignlogs:
            # MAX/MIN
            (statsmax_stats_ecb_sent, statsmin_stats_ecb_sent) = maxmin(campaignlog.stats_ecb_sent, statsmax_stats_ecb_sent, statsmin_stats_ecb_sent)
            (statsmax_stats_smtp_status, statsmin_stats_smtp_status) = maxmin(campaignlog.stats_smtp_status, statsmax_stats_smtp_status, statsmin_stats_smtp_status)
            (statsmax_stats_smtp_delivered, statsmin_stats_smtp_delivered) = maxmin(campaignlog.stats_smtp_delivered, statsmax_stats_smtp_delivered, statsmin_stats_smtp_delivered)
            (statsmax_stats_smtp_deferred, statsmin_stats_smtp_deferred) = maxmin(campaignlog.stats_smtp_deferred, statsmax_stats_smtp_deferred, statsmin_stats_smtp_deferred)
            (statsmax_stats_smtp_expired, statsmin_stats_smtp_expired) = maxmin(campaignlog.stats_smtp_expired, statsmax_stats_smtp_expired, statsmin_stats_smtp_expired)
            (statsmax_stats_smtp_bounced, statsmin_stats_smtp_bounced) = maxmin(campaignlog.stats_smtp_bounced, statsmax_stats_smtp_bounced, statsmin_stats_smtp_bounced)
            (statsmax_stats_ecb_opened, statsmin_stats_ecb_opened) = maxmin(campaignlog.stats_ecb_opened, statsmax_stats_ecb_opened, statsmin_stats_ecb_opened)
            (statsmax_stats_ecb_clicked, statsmin_stats_ecb_clicked) = maxmin(campaignlog.stats_ecb_clicked, statsmax_stats_ecb_clicked, statsmin_stats_ecb_clicked)

            reslog[campaignlog.date_started.minute] = campaignlog

        #
        ctx['nbcol'] = nbcol
        ctx['start'] = int(self.request.GET.get('start', 0))
        ctx['loop_times_th'] = []
        ctx['loop_times'] = []
        for i in range(nbcol):
            minute = (date_started_minute + i) % 60
            ctx['loop_times'].append(minute)
            if minute < 10:
                minute = '0' + str(minute)
            ctx['loop_times_th'].append(minute)
        ctx['res'] = reslog
        ctx['campaign'] = self.get_object()
        ctx['date_started'] = date_started
        ctx['date_started_real'] = date_started_real
        ctx ['statsmax_stats_ecb_sent'] = statsmax_stats_ecb_sent
        ctx ['statsmin_stats_ecb_sent'] = statsmin_stats_ecb_sent
        ctx ['statsmax_stats_smtp_status'] = statsmax_stats_smtp_status
        ctx ['statsmin_stats_smtp_status'] = statsmin_stats_smtp_status
        ctx ['statsmax_stats_smtp_delivered'] = statsmax_stats_smtp_delivered
        ctx ['statsmin_stats_smtp_delivered'] = statsmin_stats_smtp_delivered
        ctx ['statsmax_stats_smtp_deferred'] = statsmax_stats_smtp_deferred
        ctx ['statsmin_stats_smtp_deferred'] = statsmin_stats_smtp_deferred
        ctx ['statsmax_stats_smtp_expired'] = statsmax_stats_smtp_expired
        ctx ['statsmin_stats_smtp_expired'] = statsmin_stats_smtp_expired
        ctx ['statsmax_stats_smtp_bounced'] = statsmax_stats_smtp_bounced
        ctx ['statsmin_stats_smtp_bounced'] = statsmin_stats_smtp_bounced
        ctx ['statsmax_stats_ecb_opened'] = statsmax_stats_ecb_opened
        ctx ['statsmin_stats_ecb_opened'] = statsmin_stats_ecb_opened
        ctx ['statsmax_stats_ecb_clicked'] = statsmax_stats_ecb_clicked
        ctx ['statsmin_stats_ecb_clicked'] = statsmin_stats_ecb_clicked

        # domaines utilisés pour les envois
        domainvals = Spool.objects.filter(campaign=self.get_object(), domain__isnull=False).values_list('domain', flat=True).distinct()
        objdomains = []
        for domainval in domainvals:
            domain = Domain.objects.get(pk=domainval)
            if domain not in objdomains:
                objdomains.append(domain)

        ctx['domains'] = objdomains

        # statistiques d'envoi
        ctx['dsn_stats'] = Spool.objects.filter(campaign=self.get_object(),sent__isnull=False).values('campaign__id', 'smtpd_dsn').annotate(count=Count('smtpd_dsn')).order_by('smtpd_dsn')

        return ctx
