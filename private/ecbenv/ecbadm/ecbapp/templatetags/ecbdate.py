# -*- coding: UTF-8 -*-

# python
import time

# django
from django.template import Library

register = Library()

# tags

@register.filter
def formatSeconds(value):
    return time.strftime('%H:%M:%S', time.gmtime(value))
