# -*- coding: UTF-8 -*-

# python
from urllib import unquote

# django
from django.template import Library

register = Library()

# tags

@register.filter
def unquote_tpl(value):
    return unquote(value)
