# -*- coding: UTF-8 -*-

# django
from django.template.defaulttags import register

# tags

@register.simple_tag
def spoolclass(spoollog, min, max):
    if spoollog is None:
        return ''
    return spoollog.get_class_status(min, max)
