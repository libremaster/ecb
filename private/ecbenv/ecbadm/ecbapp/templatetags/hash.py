# -*- coding: UTF-8 -*-

# django
from django.template.defaulttags import register

# tags


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)
