# -*- coding: UTF-8 -*-

# python
import re

# django
from django.template.defaulttags import register

# tags


@register.filter
def get_dsn(value):
    msg = ''
    m = re.search('(\d)\.(.*)', value)
    if m:
        if m.group(1) == '2':
            msg += u'Livré'
        else:
            if m.group(1) == '4':
                msg += u'Non livré - Erreur temporaire : '
            if m.group(1) == '5':
                msg += u'Non livré - Erreur permanente : '

            if m.group(2) == '0.0':
                msg += 'Other undefined Status'
            if m.group(2) == '1.0':
                msg += 'Other address status'
            if m.group(2) == '1.1':
                msg += 'Bad destination mailbox address (left of @)'
            if m.group(2) == '1.2':
                msg += 'Bad destination system address (right of @)'
            if m.group(2) == '1.3':
                msg += 'Bad destination mailbox address syntax (left and right of @)'
            if m.group(2) == '1.6':
                msg += 'Destination mailbox has moved, No forwarding address'
            if m.group(2) == '1.8':
                msg += "Bad sender's system address"
            if m.group(2) == '1.10':
                msg += 'Recipient address has null MX'
            if m.group(2) == '2.0':
                msg += 'Other or undefined mailbox status'
            if m.group(2) == '2.1':
                msg += 'Mailbox disabled, not accepting messages'
            if m.group(2) == '2.2':
                msg += 'Mailbox full'
            if m.group(2) == '3.0':
                msg += 'Other or undefined mail system status'
            if m.group(2) == '3.1':
                msg += 'Mail system full'
            if m.group(2) == '3.2':
                msg += 'System not accepting network messages'
            if m.group(2) == '3.5':
                msg += 'System incorrectly configured'
            if m.group(2) == '4.0':
                msg += 'Other or undefined network or routing status'
            if m.group(2) == '4.1':
                msg += 'No answer from host'
            if m.group(2) == '4.2':
                msg += 'Bad connection'
            if m.group(2) == '4.3':
                msg += 'Directory server failure'
            if m.group(2) == '4.4':
                msg += 'Unable to route'
            if m.group(2) == '4.5':
                msg += 'Mail system congestion'
            if m.group(2) == '4.6':
                msg += 'Routing loop detected'
            if m.group(2) == '5.0':
                msg += 'Other or undefined protocol status'
            if m.group(2) == '5.1':
                msg += 'Invalid command'
            if m.group(2) == '5.2':
                msg += 'Syntax error'
            if m.group(2) == '5.4':
                msg += 'Invalid command arguments'
            if m.group(2) == '6.0':
                msg += 'Other or undefined media error'
            if m.group(2) == '7.0':
                msg += 'Other or undefined security status'
            if m.group(2) == '7.1':
                msg += 'Delivery not authorized, message refused'
            if m.group(2) == '7.2':
                msg += 'Mailing list expansion prohibited'
            if m.group(2) == '7.7':
                msg += 'Message integrity failure'
    return msg
