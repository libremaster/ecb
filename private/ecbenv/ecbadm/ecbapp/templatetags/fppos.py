# -*- coding: UTF-8 -*-

# django
from django.template import Library

register = Library()

# tags

@register.filter
def fppos(value):
    if value < 1:
        return 1
    else:
        return value
