# -*- coding: UTF-8 -*-

# django
from django.template.defaulttags import register

# tags

@register.simple_tag
def campaignclass(campaignlog, min, max, stats):
    if campaignlog is None:
        return ''
    return campaignlog.get_class_status(min, max, stats)

@register.simple_tag
def campaigntext(campaignlog, stats):
    if campaignlog is None:
        return ''
    return campaignlog.get_text_status(stats)
